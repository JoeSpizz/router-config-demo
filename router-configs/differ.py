import difflib
import os
from colorama import Fore, Style, init

init()  # Initialize colorama for colored output

def normalize_newline_endings(content):
    """Ensure content does not have a newline at the end."""
    return content if content[-1] != '\n' else content[:-1]

def file_diff(file1, file2):
    """Compares the contents of two files and prints the differences with color coding and descriptive text."""
    with open(file1, 'r') as f1, open(file2, 'r') as f2:
        f1_lines = normalize_newline_endings(f1.read()).splitlines(keepends=True)
        f2_lines = normalize_newline_endings(f2.read()).splitlines(keepends=True)

    diff = difflib.unified_diff(f1_lines, f2_lines, fromfile='current_config', tofile='test_config')
    diff_list = list(diff)  # Convert the generator to a list to be able to iterate over it more than once

    if not diff_list:  # If the list is empty, no differences were found
        print(Fore.GREEN + "There is no difference, good to go." + Fore.RESET)
    else:
        for line in diff_list:
            if line.startswith('+'):
                if not line.startswith('+++'):
                    print(Fore.GREEN + line + Fore.RESET, end='')
                    print(Fore.GREEN + "This line exists in test_config but not in current_config." + Fore.RESET)
                else:
                    print(Fore.GREEN + line + Fore.RESET, end='')  # File header
            elif line.startswith('-'):
                if not line.startswith('---'):
                    print(Fore.RED + line + Fore.RESET, end='')
                    print(Fore.RED + "This line exists in current_config but not in test_config." + Fore.RESET)
                else:
                    print(Fore.RED + line + Fore.RESET, end='')  # File header
            else:
                print(line, end='')

if __name__ == "__main__":
    hostname = input("Hostname of device? ")
    directory = os.path.join(hostname)  # Replace with your actual subdirectory path
    
    current_config_path = os.path.join(directory, "current_config")
    test_config_path = os.path.join(directory, "test_config")
    
    if os.path.exists(current_config_path) and os.path.exists(test_config_path):
        file_diff(current_config_path, test_config_path)
    else:
        print("One or both of the files do not exist in the specified directory.")

