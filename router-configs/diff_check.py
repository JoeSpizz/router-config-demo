import difflib
import os

def file_diff(file1, file2):
    """Compares the contents of two files and prints the differences."""
    with open(file1, 'r') as f1, open(file2, 'r') as f2:
        f1_lines = f1.readlines()
        f2_lines = f2.readlines()

    # Use difflib to get a diff
    diff = difflib.unified_diff(f1_lines, f2_lines, fromfile='current_config', tofile='next_config')

    # Check if there are differences and print them; otherwise, print no difference.
    differences = list(diff)
    if differences:
        for line in differences:
            print(line, end='')
    else:
        print("No differences found.")

if __name__ == "__main__":
    # Ask the user for the hostname, which corresponds to the directory name
    hostname = input("Hostname of device? ")
    directory = os.path.join(hostname)  # Adjust the "specific_subdirectory" to your actual subdirectory name
    
    # Define the file paths
    current_config_path = os.path.join(directory, "current_config")
    next_config_path = os.path.join(directory, "next_config")
    
    # Check if both files exist before attempting to diff them
    if os.path.exists(current_config_path) and os.path.exists(next_config_path):
        file_diff(current_config_path, next_config_path)
    else:
        print("One or both of the files do not exist in the specified directory.")

